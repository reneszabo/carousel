<?php

/*
 * Plugin Name: Move-Digital-Carousel
 * Version: 0.1
 * Description: Custom plugin move-digital-carousel
 * Author: Rene
 * License: MIT
 */

/* Add to the Menu */
add_action('admin_menu', 'register_f3_menu_page');




function register_f3_menu_page() {
  add_menu_page('Subscribers Report', 'Subscribers Report', 'manage_options', 'martino/admin/move-digital-carousel-admin.php', '', plugins_url('move-digital-carousel/images/icon.png'), 6);
}

/* Add CSS extras */
add_action('admin_enqueue_scripts', 'f3_admin_theme_style');

function f3_admin_theme_style() {
  wp_enqueue_style('martino-admin-theme', plugins_url('css/wp-move-digital-carousel-admin.css', __FILE__));
}
